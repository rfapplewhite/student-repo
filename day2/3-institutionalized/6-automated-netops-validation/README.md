## Return to Day 2 Exercises
* [Day 2 ](./day2/README.md)


# Exercise 6 - Automated Netops Validation 

## Table of Contents
- [Step 1 - Host](#step-1-host)
- [Step 2 - Create group ](#step-2-create-group)
- [Step 3 - Staging Branch](#step-3-staging-branch)
- [Step 4 - Tunnel](#step-4-tunnel)
- [Step 5 - Persist](#step-5-persist)
- [Step 6 - Configuration](#step-6-configuration)
- [Step 7 - Deploy](#step-7-deploy)
- [Step 8 - Health](#step-8-health)
- [Step 9 - Merge to main](#step-9-merge-to-main)
- [Step 10 - Workflow](#step-10-workflow)

## Solution
If needed.
[solution](solution/)

## Objective

In this Capstone project you will connect the routers in your assigned RHPD student pod to a Cloud router that is running in a sparate VPC in AWS. For this execrise the AWS automation is already completed and the cloud router is available to connect from your POD. The instructor will provide credentials to access the cloud roter. You will need to take care to use the proper configurations and IP addreses assigned to your POD based on your assigned student number. It is possible to effect somebody else's configuration on the cloud_rtr. Oh dang, did it just get real??.....


## Overview

This exercise will focus on NetOps and GitOps using a Staging Branch. You will build on some of the previous BGP configuration from exercise 5. If you are missing some of the network prefixes, no worries - there is an opportunity to fix any omitions and make additions during the Capstone lab steps. The idea is to create configurations and validate them in the Staging branch. Once we have a good "intended" configuration, we can merge our changes in the student-repo from staging to the main branch. Subsequently we will create a workflow that we would use to push the changes into production. In this exercise we don't have another inventory for production device IP addresses, so we will simply run the main branch using our existing student pod environment. For example, if we had another set of production routers, our workflow could prompt for which inventory to select (staging or production).

### Step 1 - Host
Create a new host for the cloud_rtr in the Workshop Inventory using the AAP GUI

1. Add these host variables in the GUI with YAML:
~~~
---
ansible_user: cloud
ansible_password: <provided by the instructor>
ansible_host: 3.139.48.143
~~~

### Step 2 - Create group 
Create a new group named `cloud` in the Workshop Inventory using the AAP GUI

1. Add these group variables in the GUI with YAML:
~~~
---
ansible_connection: network_cli
ansible_network_os: cisco.ios.ios
~~~

2. Add the `cloud_rtr` to the cloud group and make sure the host is enabled.

 ![cloud](../../images/enable_cloud_rtr.png)  


### Step 3 - Staging Branch
As with Devops best practices and Netops/GitOps in particular, it's paramount to incorporate automated testing and validation into our automated pipelines. In this Capstone project we will initially build and test our automation for adding BGP and connectivity to a cloud_rtr in our staging "lab" environment. The idea is to first determine the configuration baseline and appropriate testing in the staging "Lab" environment prior to configuring the actual production network. A staging branch referes to source control for our initial playbooks, templates, and configurations as code. Once we have validated the "code" and changes applied to our staging lab environment, we can subsequently merge the "code" to our main branch, which is intended to be applied to the actual production environement during a future change window.

1. Checkout a new branch called staging from the vscode terminal.
~~~
$ cd day2/3-institutionalized/6-automated-netops-validation/
$ git checkout -b staging
~~~
At this point all of our additions and changes made to the scm are in the staging branch.

2. Temporaily change the Student Project in AAP to the `staging branch`. Also set the `branch override` option.
Why do this? Let's say you create a new playbook or modify an existing playbook in the staging branch but it doesn't show when your project syncs in AAP. This is because the project will pull from the main branch by default unless explicitly added to the project.

 ![staging](../../images/student_project_staging.png) 

#### Step 4 - Tunnel
Some configurations are not `yet` supported directly with resource modules. These types of one-off configurations are normally addressed using the config moudles with the lines parameter or jinja2 templates. Please review the playbook `day2/3-institutionalized/6-automated-netops-validation/tunnel.yml` This playbook will configure our tunnel interface source and destination settings for the BGP connection between rtr1 and the cloud_rtr.

1. Create a job-template named Network-Capstone-Tunnel with the following parameters. 

 - name: **Network-Capstone-Tunnel**
 - organization: **Red Hat network organization**
 - inventory: **Workshop Inventory**
 - project: **Student Project**
 - playbook: **day2/3-institutionalized/6-automated-netops-validation/solution/tunnel.yml**
 - ask_scm_branch_on_launch: **true**
 - ask_variables_on_launch: **true**
 - variables:
   #Variables for Playbook
 - student_number: **''**
 - credentials:
   - **Workshop Credential**
 - execution_environment: **Validated Network**

![tunnel](../../images/tunneljobtemplate.png)       
  
2. Run the job-template named `Network-Capstone-Tunnel` and provide staging for the branch and the student_number. Take care to provide the correct assigned student number!


### Step 5 - Persist
The resource_manager role uses the persist action to get the facts for a given resource and store it as inventory host_vars. 
This time we will refine the resource list to bgp resources and injest the cloud router configuration. 

1. Create a job-template named Network-Capstone-Persist with the following parameters. Please note that we will reuse the gitlab token created back in exercise 1. Hence this job-template will have two credentials assigned. The machine credential "Workshop Credential" and the Gitlab Student credential type with the "Gitlab Credential".

* Paremeters:
- name: **Network-Validated-Persist**
- organization: **Red Hat network organization**
- inventory: **"Workshop Inventory**
- project: **"Student Project**
-  playbook: **"day2/3-institutionalized/6-automated-netops-validation/persist.yml"**
- ask_scm_branch_on_launch: **true**
- ask_variables_on_launch: **true**
- variables:
    scm_branch: **''**
- credentials:
  - **"Workshop Credential"**
  - **"Gitlab Credential"**
- execution_environment: **"Validated Network"**

2. Review the `persist.yml` playbook located `~/student-repo/day2/3-institutionalized/6-automated-netops-validation/persist.yml`
- Notice how the gitlab variables are defined in vars/git.yml and the Gitlab Credential for the token. Secondly the resource modules are listed in the vars/resource for the play. For this exercise only the two BGP resources are used.
 

3. Launch the `Network-Validated-Persist` job template and review the output. Pay close attention to the two prompts and provide "staging" for both the job-template and playbook extra variable `scm_branch`.

4. Verify the host_vars entries for each of the four routers in the day2/3-institutionalized/6-automated-netops-validation/host_vars of your gitlab student-repo project.

- day2/day2/3-institutionalized/6-automated-netops-validation/
 ![hostvars](day2/images/hostvars_persist2.png)  


### Step 6 - Configuration
In this section you need to modify the host_vars for the cloud_rtr and rtr1. Also verify the networks from Exercise 5 are in the the configurations for rtr1 and rtr2.

1. Review the configs from cloud-rtr
* Some of the configs such as the vrf and loopback for your student_number have been pre-configured. Take care to use your student_number for the BGP configurations for the cloud_rtr. Mistakes could impact another students configuration. Student1 is reserved for the instructor and provided as an example to guide your confguration for your assigned student_number.

The cloud_rtr an be accessed by ssh. The instructor will provide credentials.
~~~
ssh cloud@3.139.48.143
~~~

#### cloud_rtr important configs:
```
# completed
! vrf definition student1
 rd 65501:1
 !
 address-family ipv4
 exit-address-family
!
vrf definition student10
 rd 65501:10
 !
 address-family ipv4
 exit-address-family
!
vrf definition student11
 rd 65501:11
 !
 address-family ipv4
 exit-address-family
!
vrf definition student12
 !        
 address-family ipv4
 exit-address-family
!
vrf definition student13
 rd 65501:13
 !
 address-family ipv4
 exit-address-family
!
vrf definition student14
 rd 65501:14
 !
 address-family ipv4
 exit-address-family
!
vrf definition student15
 rd 65501:15
 !
 address-family ipv4
 exit-address-family
!
vrf definition student16
 rd 65501:16
 !
 address-family ipv4
 exit-address-family
!
vrf definition student17
 rd 65501:17
 !
 address-family ipv4
 exit-address-family
!
vrf definition student18
 rd 65501:18
 !
 address-family ipv4
 exit-address-family
!
vrf definition student19
 rd 65501:19
 !
 address-family ipv4
 exit-address-family
!
vrf definition student2
 rd 65501:2
 !
 address-family ipv4
 exit-address-family
!
vrf definition student20
 rd 65501:20
 !
 address-family ipv4
 exit-address-family
!
vrf definition student3
 rd 65501:3
 !
 address-family ipv4
 exit-address-family
!
vrf definition student4
 rd 65501:4
 !
 address-family ipv4
 exit-address-family
!         
vrf definition student5
 rd 65501:5
 !
 address-family ipv4
 exit-address-family
!
vrf definition student6
 rd 65501:6
 !
 address-family ipv4
 exit-address-family
!
vrf definition student7
 rd 65501:7
 !
 address-family ipv4
 exit-address-family
!
vrf definition student8
 rd 65501:8
 !
 address-family ipv4
 exit-address-family
!
vrf definition student9
 !
 address-family ipv4
 exit-address-family
#completed
!
interface Loopback0
 ip address 192.168.100.100 255.255.255.255
!
interface Loopback1
 vrf forwarding student1
 ip address 192.168.10.1 255.255.255.255
!
interface Loopback2
 vrf forwarding student2
 ip address 192.168.10.2 255.255.255.255
!
interface Loopback3
 vrf forwarding student3
 ip address 192.168.10.3 255.255.255.255
!
interface Loopback4
 vrf forwarding student4
 ip address 192.168.10.4 255.255.255.255
!
interface Loopback5
 vrf forwarding student5
 ip address 192.168.10.5 255.255.255.255
!
interface Loopback6
 vrf forwarding student6
 ip address 192.168.10.6 255.255.255.255
!
interface Loopback7
 vrf forwarding student7
 ip address 192.168.10.7 255.255.255.255
!
interface Loopback8
 vrf forwarding student8
 ip address 192.168.10.8 255.255.255.255
!
interface Loopback9
 vrf forwarding student9
 ip address 192.168.10.9 255.255.255.255
!
interface Loopback10
 vrf forwarding student10
 ip address 192.168.10.10 255.255.255.255
!
interface Loopback11
 vrf forwarding student11
 ip address 192.168.10.11 255.255.255.255
!
interface Loopback12
 vrf forwarding student12
 ip address 192.168.10.12 255.255.255.255
!         
interface Loopback13
 vrf forwarding student13
 ip address 192.168.10.13 255.255.255.255
!
interface Loopback14
 vrf forwarding student14
 ip address 192.168.10.14 255.255.255.255
!
interface Loopback15
 vrf forwarding student15
 ip address 192.168.10.15 255.255.255.255
!
interface Loopback16
 vrf forwarding student16
 ip address 192.168.10.16 255.255.255.255
!
interface Loopback17
 vrf forwarding student17
 ip address 192.168.10.17 255.255.255.255
!
interface Loopback18
 vrf forwarding student18
 ip address 192.168.10.18 255.255.255.255
!
interface Loopback19
 vrf forwarding student19
 ip address 192.168.10.19 255.255.255.255
!
interface Loopback20
 vrf forwarding student20
 ip address 192.168.10.20 255.255.255.255
!
# completed with the tunnel.yml playbook
interface Tunnel1
 description student1
 vrf forwarding student1
 ip address 10.250.1.1 255.255.255.0
 tunnel source GigabitEthernet1
 tunnel destination 3.147.86.139
!
interface Tunnel2
 description student2
 vrf forwarding student2
 ip address 10.250.2.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel3
 description student3
 vrf forwarding student3
 ip address 10.250.3.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel4
 description student4
 vrf forwarding student4
 ip address 10.250.4.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel5
 description student5
 vrf forwarding student5
 ip address 10.250.5.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel6
 description student6
 vrf forwarding student6
 ip address 10.250.6.1 255.255.255.0
 tunnel source GigabitEthernet1
!         
interface Tunnel7
 description student7
 vrf forwarding student7
 ip address 10.250.7.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel8
 description student8
 vrf forwarding student8
 ip address 10.250.8.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel9
 description student9
 vrf forwarding student9
 ip address 10.250.9.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel10
 description student10
 vrf forwarding student10
 ip address 10.250.10.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel11
 description student11
 vrf forwarding student11
 ip address 10.250.11.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel12
 description student12
 vrf forwarding student12
 ip address 10.250.12.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel13
 description student13
 vrf forwarding student13
 ip address 10.250.13.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel14
 description student14
 vrf forwarding student14
 ip address 10.250.14.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel15
 description student15
 vrf forwarding student15
 ip address 10.250.15.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel16
 description student16
 vrf forwarding student16
 ip address 10.250.16.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel17
 description student17
 vrf forwarding student17
 ip address 10.250.17.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel18
 description student18
 vrf forwarding student18
 ip address 10.250.18.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel19
 description student19
 vrf forwarding student19
 ip address 10.250.19.1 255.255.255.0
 tunnel source GigabitEthernet1
!
interface Tunnel20
 description student20
 vrf forwarding student20
 ip address 10.250.20.1 255.255.255.0
 tunnel source GigabitEthernet1
!

!
router bgp 65501
 bgp router-id 192.168.100.100
 bgp log-neighbor-changes
 redistribute connected
 !
 #TODO complete in host_vars/ 
 address-family ipv4 vrf student1
  bgp router-id 192.168.10.1
  network 10.250.1.0 mask 255.255.255.0
  network 192.168.10.1 mask 255.255.255.255
  redistribute connected
  neighbor 10.250.1.2 remote-as 65000
  neighbor 10.250.1.2 ebgp-multihop 255
  neighbor 10.250.1.2 activate
 exit-address-family
 !
 address-family ipv4 vrf student10
  bgp router-id 192.168.10.10
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student11
  bgp router-id 192.168.10.11
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student13
  bgp router-id 192.168.10.13
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student14
  bgp router-id 192.168.10.7
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student15
  bgp router-id 192.168.10.15
  redistribute connected
 exit-address-family
 !        
 address-family ipv4 vrf student16
  bgp router-id 192.168.10.16
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student17
  bgp router-id 192.168.10.17
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student18
  bgp router-id 192.168.10.18
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student19
  bgp router-id 192.168.10.19
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student2
  bgp router-id 192.168.10.2
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student20
  bgp router-id 192.168.10.20
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student3
  bgp router-id 192.168.10.3
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student4
  bgp router-id 192.168.10.4
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student5
  bgp router-id 192.168.10.5
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student6
  bgp router-id 192.168.10.6
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student7
  bgp router-id 192.168.10.7
  redistribute connected
 exit-address-family
 !
 address-family ipv4 vrf student8
  bgp router-id 192.168.10.8
  redistribute connected
 exit-address-family
```

2. Modify the `day2/day2/3-institutionalized/6-automated-netops-validation/host_vars/cloud_rtr/bgp_address_family.yaml`
~~~
networks:
        -   address: 10.250.<your_student_numbet>.0
            mask: 255.255.255.0
        -   address: 192.168.<your_student_numbet>.1
            mask: 255.255.255.255
~~~

3. Modify the `day2/day2/3-institutionalized/6-automated-netops-validation/host_vars/rtr1/bgp_address_family.yaml`
~~~
networks:
        -   address: 10.100.100.0
            mask: 255.255.255.0
        -   address: 10.200.200.0
            mask: 255.255.255.0
        -   address: 172.16.0.0
        -   address: 192.168.1.1
            mask: 255.255.255.255
        -   address: 192.168.3.3
            mask: 255.255.255.255
        -   address: 10.250.<your_student_number>.0
            mask: 255.255.255.0
~~~

4. Modify the `day2/day2/3-institutionalized/6-automated-netops-validation/host_vars/rtr1/bgp_global.yaml`
~~~
neighbors:
    -   neighbor_address: 10.200.200.2
        remote_as: '65001'
    -   neighbor_address: 10.250.<your_student_number>.1
        remote_as: '65501'
~~~

5. Verify rtr2 is configured. 
Modify the `day2/day2/3-institutionalized/6-automated-netops-validation/host_vars/rtr2/bgp_address_family.yaml`
~~~
 network:
        -   address: 10.101.101.0/24
        -   address: 10.200.200.0/24
        -   address: 172.18.0.0/16
        -   address: 192.168.2.2/32
        -   address: 192.168.4.4/32
~~~

6. Save all files and commit and push in the staging branch.

Complete the git steps for your change. You must save, commit the file in the VSCode IDE and "sync" push to gitlab after fixing the appropriate host_vars files.
![Save](day2/images/save_commit.png)

or update from the terminal
~~~
git add --all
git commit -m "capstone"
git push
~~~


### Step 7 - Deploy
The resource_manager role uses the Deploy action to apply changes to the listed resources. The deploy action works well for additions to the device's configuration because it uses the state of merged. The state of merged will not overwrite any existing configurations.

4. Create the `Network-Capstone-Deploy` job-template with the following parameters.
Hint, you can copy the Network-Validated-Persist job-template and slighlty modify.

Parameters:
- name: **Network-Capstone-Deploy**
- organization: **"Red Hat network organization"**
- inventory: **"Workshop Inventory"**
- project: **"Student Project"**
- playbook: **"day2/3-institutionalized/6-automated-netops-validation/deploy.yml"**
- ask_scm_branch_on_launch: **true**
- ask_variables_on_launch: **true**
- variables:
    scm_branch: **''**
- credentials:
    - **"Workshop Credential"**
    - **"Gitlab Credential"**
- execution_environment: **"Validated Network"**

5. Launch the N`etwork-Capstone-Deploy` job-template


### Step 8 - Health
The health.yml playbook can validated the BGP neighbor adjacency and connectiivty to the loopback interfaces.

cloud_rtr should be able to ping:
- 192.168.1.1
- 192.168.2.2
- 192.168.3.3
- 192.168.4.4

rtr1 should be ale to ping the the cloud_rtr loobback:
- 192.168.10.<your_student_number>

1. Create the Network-Capstone-Health job-template with the following parameters.

Parameters:
- name: **Network-Capstone-Health**
- organization: **"Red Hat network organization"**
- inventory: **"Workshop Inventory"**
- project: **"Student Project"**
- playbook: **"day2/3-institutionalized/6-automated-netops-validation/solution/health.yml"**
- ask_scm_branch_on_launch: **true**
- ask_variables_on_launch: **true**
- variables:
    student_number: **''**
- credentials:
  - **"Workshop Credential"**
- execution_environment: **"Validated Network"**

5. Launch the Network-Capstone-Health  job-template
Ensure that both of the neighbors and loobback connectivity test succeed. If not, troubleshoot...

## Step 9 - Merge to Main
You are done with the staging branch if you have sucessfully configured and ran all of the aforementioned job-templates. 

1. Modify the student_project in the AAP to use the "default" main branch.

2. Make sure you have pushed all committed changes on the staging branch

3. Access your student-repo on gitlab.com and complete the following merge request process.

![Save](../../images/merge1.png)


![Save](../../images/merge2.png)


![Save](../../images/merge3.png)


![Save](day2/images/merge4.png)


![Save](../../images/merge5.png)

- We remove the staging branch and checkout the main branch from the terminal here. 
- If we need it again we would check it out as a new branch from our local git.

~~~
$ git checkout main
~~~

## Step 10 - Workflow
In this step you will create a workflow to deploy and test BGP configuration changes into production.

The completed workflow will look like the following:
![capstone](../../images/capstoneworkflow.png)

1. Configure the workflow with the following parameters:
-  name: **Network-Capstone-Workflow**
-  inventory: **"Workshop Inventory"**
-  organization: **"Red Hat network organization"**
- ask_scm_branch_on_launch: **true**
- ask_variables_on_launch: **true**
- variables:
    scm_branch: **''**
    student_number: **''**
       
2. Nodes
    - name: **Node Network-Capstone-Deploy**
    - name: **Network-Capstone-Health**
      
3. Luanch the workflow
This workflow will run against the same devices in you RHPD Pod and cloud router. Since you ran it in staging earlier it should also pass with the workflow.

* Makesure you include the main branch and the correct student number!
![capstone](../../images/launchcapstone.png)

* Verify the workflow succeeeded
![capstone](../../images/capstone_done.png)

- If needed a setup.yml with associated playbooks and hostvars are already available in  ~/student-repoday2/3-institutionalized/6-automated-netops-validation/solution/

#### Congratulations on completing the Capstone project!!! Not only did you establish a single source of truth for the BGP configurations, you are the TRUTH!!!!!!

#DONE

[Click Here to return to the Ansible Network Automation Workshop](../README.md)





